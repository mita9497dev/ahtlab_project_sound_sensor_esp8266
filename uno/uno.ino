#include <SoftwareSerial.h>

SoftwareSerial esp(8, 9);

#define AmpMax (1024 / 2)
#define MicSamples (1024*2)
#define MIC A0
#define VolumeGainFactorBits 0
#define limitTime 50
//#define ADCFlow 1

#define led 3

#define SEND_TIMEOUT 1000L

unsigned long lastTimeSend = 0;

void setup()
{
    Serial.begin(9600);
    delay(1000);
    
    esp.begin(9600);
    delay(1000);

    pinMode(led, OUTPUT);
    
    Serial.println(0);
}

void loop()
{
    float val = readSensor();
    if (millis() - lastTimeSend > SEND_TIMEOUT)
    {
        sendESP(val);
        lastTimeSend = millis();
    }
}

float readSensor()
{
    return MeasureVolume();
//    return random(10, 20) + (float)(random(0, 100))/100;
}

// Gui qua esp
void sendESP(float value)
{
    Serial.println(value);
    
    esp.print(value);
    esp.print('$');
}

// Doc sensor
int MeasureVolume()
{
    int dB, adc;
    
    unsigned long x = millis(); // khai báo biến x

    int signalx = 0;
    int signalMax = 0; //  tín hiệu lớn nhất
    int signalMin = 1000; // tín hiệu nhỏ nhất

    ///////////////////// phần xử lý tín hiệu âm thanh vào////////////////////////
    while (millis() - x < limitTime) // thực hiện vòng lặp 50ms lọc ra 50 tín hiệu adc
    {
        signalx = analogRead(MIC); // đọc tín hiệu adc input
        if (MIC < 1500) {
            if (signalx > signalMax) {
                signalMax = signalx; // chọn tín hiệu có giá trị lớn nhất
            } else if (signalx < signalMin) {
                signalMin = signalx; // chọn tín hiệu có giá trị nhỏ nhất
            }
        }
    }
    ///////////////////// phần xử lý tín hiệu âm thanh vào////////////////////////
    adc = signalMax - signalMin; // giá trị adc sau cùng thu dc 

    //  thực hiện phương pháp Hồi Quy Tuyến Tính
    // ADC = dB*9.0316 + 180.6 ;
    dB = (adc + 17.298) / 9.00028; // 

    if (dB > 70) {
        digitalWrite(led, HIGH);
    } else {
        digitalWrite(led, LOW);
    }
    
    return dB;
}
