#include <ESP8266WiFi.h>
#include <WiFiClient.h> 
#include <ESP8266HTTPClient.h>

#define SSID "AN BINH"
#define PASS "anbinhbio.com"

#define SEND_TIMEOUT 1000
unsigned long lastTimeSend = 0;

#include <SoftwareSerial.h>

SoftwareSerial uno(D7, D8);
String data = "";

void setup()
{
    Serial.begin(9600);
    delay(1000);

    uno.begin(9600);
    delay(1000);

    WiFI_Init();
}

void loop()
{
    while(uno.available())
    {
        char c = uno.read();
        data += c;

        Serial.print(c);
    }

    if (millis() - lastTimeSend > SEND_TIMEOUT)
    {
        // data co dang: 16.53$17.49$12.20$14.22$16.59$
        // gui xong: data = ""
        Serial.println("\nSend data to server");
        sendServer();

        lastTimeSend = millis();
    }
    
    delay(1000);
}

// Cai dat wifi
void WiFI_Init()
{
    WiFi.begin(SSID, PASS);     //Connect to your WiFi router
    Serial.println("");
    
    Serial.print("Connecting");
    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
    }
}

void sendServer()
{
    HTTPClient http;
    // dia chi server + chuoi data
    String url = "http://raoline.com/noise_sensor/?s=" + data;
    
    http.begin(url);     //Specify request destination
    
    int httpCode = http.GET();            //Send the request
    String payload = http.getString();    //Get the response payload
    
    Serial.println(httpCode);   //Print HTTP return code
    Serial.println(payload);    //Print request response payload
    
    http.end();  //Close connection

    // reset data
    data = "";
}
