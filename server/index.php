<?php
date_default_timezone_set('Asia/Ho_Chi_Minh');

require 'firebase-php-master/src/firebaseLib.php';

define('DEFAULT_URL', 'https://ahtlab-ota.firebaseio.com/');
define('DEFAULT_TOKEN', 'enF2oXThE7aTe72KQKgS5RNoZz3d8W38fmYC1GsC');
define('DEVICE_PATH', 'devices/d01');
define('DATA_PATH', DEVICE_PATH . '/data');

if (!empty($_GET['s'])) {
    $data = trim($_GET['s']);
    $arr = explode('$', $data);
    $timestamp = time();

    $firebase = new \Firebase\FirebaseLib(DEFAULT_URL, DEFAULT_TOKEN);
    
    $config = json_decode($firebase->get(DEVICE_PATH . '/' . 'config'));

    $min = $config->min ? $config->min : 140;
    $max = $config->max ? $config->max : 0;
    $cal = floatval($firebase->get(DEVICE_PATH . '/' . 'calib'));

    
    $len = count($arr);
    $i = 0;
    foreach ($arr as $val) {
        if ($val === '') continue;

        $val += $cal;
        
        $val = floatval($val);
        if ($val > 60) {
            $config->alert = 'Cảnh báo: vượt ngưỡng';
        } else {
            $config->alert = '';
        }

        if ($val < $min) {
            $min = $val;
        }
        if ($val > $max) {
            $max = $val;
        }

        $firebase->set(DATA_PATH . '/' . ($timestamp - $len + $i++), $val);
    }
    $config->min = $min;
    $config->max = $max;
    $firebase->set(DEVICE_PATH . '/' . 'config', json_decode(json_encode($config), true));

    echo 'succcess';
}